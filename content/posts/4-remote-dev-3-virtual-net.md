+++
author = "Mateusz Urbanek"
title = "My remote development tools (part 3) - Virtual Network"
date = "2020-10-29"
description = "Remote development setup with ZeroTier One and Tailscale."
tags = [
    "network",
    "remote-dev",
]
ShowToc = true
+++

Last part of mine remote development kit, is virtual software defined network. I don't have access to public IP, so I couldn't setup VPN. The solution was quite simple - virtual software defined network. My goal was to find something free or freemium and open source. Firstly I stumbled upon ZeroTier One and later I found Tailscale. Both seemed to fulfill my needs, so I will write something about both of them, although do not consider me as specialist - those will be only my thoughts on those pieces of software.

# ZeroTier One

![zerotier](https://assets.digitalocean.com/articles/zerotier-1604/ZeroTierSettings-updated.png)

This was my first, initial choice. Setup on Windows was extremely easy - I've installed the client, and from the taskbar, after right clicking the icon I've selected *Join network*. Then I provided network ID that was given to my network on the web control panel, and I was nearly good to go. Last step was to set my device as authorized on the control panel.

## Command Line Interface

This one was even simpler than the Windows configuration. I installed it with default, less secure option on my Docker host machine with Ubuntu Server 20.04 LTS:

```bash
curl -s https://install.zerotier.com | sudo bash
```

There is also more secure option available:
```bash
curl -s 'https://raw.githubusercontent.com/zerotier/ZeroTierOne/master/doc/contact%40zerotier.com.gpg' | gpg --import && \
if z=$(curl -s 'https://install.zerotier.com/' | gpg); then echo "$z" | sudo bash; fi
```
After installation I just typed `zerotier-cli join network-id` and my device connected to the virtual network. The last step was identical like in the Windows configuration - authenticate device on the web interface.

# Tailscale

![tailscale](https://tailscale.com/static/images/marketing/home-illustration-1040w.png)

Installation of Tailscale was as painless as installation of ZeroTier One. The key difference was that after installation when prompted to login to my account, the website with Google's OAuth2 was opened, and I was able to login.

## Command Line Interface

This was little bit more painful than the installation of ZeroTier-CLI package. I had to run only a few commands, but the server hosting the package was much slower, and this resulted with much longer download time.

```bash
curl -fsSL https://pkgs.tailscale.com/stable/ubuntu/eoan.gpg | sudo apt-key add -
curl -fsSL https://pkgs.tailscale.com/stable/ubuntu/eoan.list | sudo tee /etc/apt/sources.list.d/tailscale.list

sudo apt-get update
sudo apt-get install tailscale

sudo tailscale up
```

After last command, I received website address, that I had to visit to authenticate the machine - I once again logged using Google's OAuth2. Additionally I could set expiration of key to never or to default value (default expiration time is equal to 6 months).

# Conclusions

Overall, both both solutions get the job done. Both feature configuration with JSON files (although ZeroTier uses custom parser) and simple GUI. Although some features of ZeroTier CLI seems to be more advanced, I prefer the Tailscale. It has cleaner, simpler interface, and the taskbar icon looks *Just Right™*. I use mostly default configuration, as it does everything I need.

# Resources

1. ZeroTier - [zerotier.com](https://www.zerotier.com/)
1. Tailscale - [tailscale.com](https://tailscale.com/)