+++
author = "Mateusz Urbanek"
title = "My remote development tools (part 2) - Visual Studio Code"
date = "2020-10-21"
description = "Remote development setup with VSCode editor."
tags = [
    "vscode",
    "remote-dev",
    "visual-studio",
]
ShowToc = true
+++

Visual Studio Code is an Electron-based app with large community support. It was initially developed as a fork of Atom - an open-source code editor developed by GitHub. As it's Electron-based, there is little overhead compared to other code editors like Sublime Text 3, Kate, or Notepad++. But its biggest strength is customizability. It supports a huge number of community developed plugins and themes. I will focus mostly on the ones used by me.

# Theme? Classic but advanced

My theme of choice is [dark-plus-syntax](https://marketplace.visualstudio.com/items?itemName=dunstontc.dark-plus-syntax). It is based on the original, official Dark Plus theme for Visual Studio Code, but it improves it and extends. It doesn't fully fit into my aesthetics (honestly my go-to color scheme is the one used by [The Cherno](https://www.youtube.com/user/TheChernoProject), but it's not available for VS Code), but the way it extends the syntax highlighting just amazes me. Everything is right in its place, colored exactly as it should be.

![d-p-s](https://www.vscolors.com/img/1280/f66a2b20d49315239e6464170aa33814.png?2)
*Image 1: dark-plus-syntax - my color scheme of choice*

We should also note that the theme is not the only thing that helps you recognize fragments of your project at a glance. I'm using [Material Icon Theme](https://marketplace.visualstudio.com/items?itemName=PKief.material-icon-theme). This one is simple and colorful while containing a large number of icons for multiple languages and filetypes.

![mat-ico-theme](https://repository-images.githubusercontent.com/67831372/23a66080-b0ef-11ea-83cc-0ab232ccf83a)
*Image 2: Material Icon Theme - examples of some icons*

# What about those extensions?

Main extensions that are crucial for remote work are included in [Remote Developement](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.vscode-remote-extensionpack) extension pack made by Microsoft. It consists of 3 plugins described below.

## Remote - SSH
![ssh](https://ms-vscode-remote.gallerycdn.vsassets.io/extensions/ms-vscode-remote/remote-ssh/0.55.0/1600016540525/Microsoft.VisualStudio.Services.Icons.Default)

This extension allows me to connect through SSH to my dev server. It can be a virtual machine, physical workstation, or even Docker container (with sshd, as I showed it in the [previous post](https://shanduur.github.io/posts/1-remote-dev-1-docker/)). That allows us to run Visual Studio Code backend and store our code in the cloud, remote location, and access it and edit whenever we have internet access.

## Remote - WSL
![wsl](https://ms-vscode-remote.gallerycdn.vsassets.io/extensions/ms-vscode-remote/remote-wsl/0.51.0/1603208343221/Microsoft.VisualStudio.Services.Icons.Default)

This one is aimed at developers using Windows 10. As you can probably know, WSL is a compatibility layer for running Linux binary executables (in Executable and Linkable Format) natively on Windows 10 and Windows Server 2019. Since WSL2 was introduced in May 2019, the performance of the subsystem increased noticeably, due to the use of Hyper-V Type 1 hypervisor. This extension allows us to develop a Linux application directly on Windows. It also isolates them (at least isolates its dependencies) from our primary system.

## Remote - Containers
![containers](https://ms-vscode-remote.gallerycdn.vsassets.io/extensions/ms-vscode-remote/remote-containers/0.145.1/1603091817684/Microsoft.VisualStudio.Services.Icons.Default)

You don't have access to a remote workstation, and you are using Linux or macOS (or maybe FreeBSD)? Or your internet connection is unstable? Then Docker and this extension are your best friends. You don't need to set up any specific container, just get the one with the development environment you need and use it. It is using neither an external connection nor excessive internet traffic.

## Noticeable mentions

Here's also a list of extensions that can be useful while developing software using Visual Studio Code:

- [GitLens](https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens) - it helps to visualize code authorship at a glance via Git blame annotations and code lens, allows seamless navigation and exploration of Git repositories, and gives you a way to gather valuable insights via powerful comparison commands (and much more).
- [Numbered Bookmarks](https://marketplace.visualstudio.com/items?itemName=alefragnani.numbered-bookmarks) - it allows us to set up bookmarks right in our files containing source code and jump to them with a simple keyboard shortcut.

# Resources
1. Visual Studio Code - [visualstudio.com](https://code.visualstudio.com/)
1. dark-plus-syntax - [github.com/dunstontc](https://github.com/dunstontc/dark-plus-syntax)
1. Material Icon Theme - [github.com/PKief](https://github.com/PKief/vscode-material-icon-theme)
