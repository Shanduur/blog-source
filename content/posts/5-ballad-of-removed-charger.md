+++
author = "Mateusz Urbanek"
title = "The Ballad of removed Charging Brick"
date = "2020-10-30"
description = "My point of view on removing charging brick and environmetnalism"
tags = [
    "apple",
    "environmentalism",
]
ShowToc = true
+++

A few weeks ago, on October 13, Apple presented its new smartphones - iPhone 12 Mini, iPhone 12, iPhone 12 Pro, and iPhone 12 Pro Max. The Pro series was just a small incremental update over last year's prosumer phones. Upgrade in the consumer series is quite significant - we now have a better screen, with a bigger DPI than the previous year's iPhone 11. What shocked most of the people was information about removing the charging brick, due to reducing the amount of waste produced by the company every year. In my opinion, this was the right move, but it is overshadowed by something else...

![ip12-12pro](https://cnet4.cbsistatic.com/img/YQlXeeHCk-e--zjAOA19Cc-ISPs=/940x0/2020/10/18/bdb7ea97-cb99-48d8-a69c-38d26109f33b/05-iphone-12-pro-2020.jpg)
*iPhone 12 and iPhone 12 Pro - CNET*

# Removing charger - a better decision?

Yes! As Lisa Jackson from Apple stated, there are 2 billion Apple power adapters in the World. This doesn't include third-party chargers. Just ask yourselves how many chargers do you have? I own at least 4 chargers - from a simple 5W USB-A Apple charger to 30W BlitzWolf USB-C Power Delivery chargers. This also means that the packaging is smaller, and that wastes less paper. Also, I can assure you that even if now Samsung and Xiaomi [are mocking Apple for removing charger](https://www.youtube.com/watch?v=4E7ybMkEZLE), they will do the same thing in the nearest future.

## USB-C cable in the box

{{< youtube Sx6dAx7dnXg >}}

This is the first problem. Most iPhone chargers came with a USB-A port that is much bigger and, what's most important, not compatible with USB-C. Is there any other solution to this situation? I can see at least a few.

## Possibility to add charger during checkout

Buying additional chargers during checkout? Yes. I know this sounds ridiculous, but adding the charger for free is just stupid! This will mean that nearly everybody will get the charger *because it's free*. This is also the best moment to start producing more uniform, future proof GaN USB Power Deliver chargers, starting at 30W, maybe with multiple USB-C ports?

## USB-A to USB-C adapter in the box

What if Apple included [USB-A plug to USB-C port adapter](https://www.amazon.com/Upgraded-Basesailor-Compatible-Chargers-Standard/dp/B079LYHNSR) in the box? Even if this sounds stupid, we would be assured that chargers that we have in our drawers won't be totally useless!

# Dark clouds

This thing just foreshadowed everything else Apple did. This is just purely outrageous for me how they are pushing their anti repair designs... The iPhone internally is pretty modular as [iFixIt teardown video shown](https://www.youtube.com/watch?v=DDV0_fDJZ40). So this means we could go to any 3rd party repair shop and get everything fixed, from a broken screen, through a bad battery, up to a broken camera, am I right? No. Even genuine Apple parts, starting from the battery up to the rear camera, are paired with the motherboard. Don't you believe me? Watch this video by Hugh Jeffreys:

{{< youtube FY7DtKMBxBw >}}

## Right to repair

As I can understand why they decided to disable FaceID or TouchID on non-genuine parts, as this can mean a threat to the security of the users' data, what's the point of deactivating the rear camera or even some display or battery features after 3rd party repair? Even when the parts are genuine! Why can't they just leave the features turned on and reduce the amount of fully functional devices being thrown away because repair will reduce the number of features. Imagine how much e-waste will be reduced. The problem appears when we realize that this will mean lower sales for the manufacturers. But this situation will force them to innovate, to improve the products that much that we, consumers, will want to buy a new device.

## Did you ever hear the tragedy of Magsafe?

![magsafe](https://photos5.appleinsider.com/gallery/38468-73114-201026-MagSafe-xl.jpg)

With the introduction of Magsafe standard on the new iPhone, we were so close to creating something so revolutionary and well designed, almost perfect... Then the news hit us right in the face. While being Qi-compatible, the 15W charging mode is restricted only to the new Apple 20W wall plug. And the time needed to charge older Apple phones is slower than using a genuine and slow 5W USB charger! It also will not charge non-Apple devices at full speed. Making MagSafe fully Qi-compatible without quirks like restricting it to single charger brick would change the way we charge our iPhone forever. Now besides buying iPhone and a MagSafe wireless charger, the user is forced to buy a specific 20W charger (and can't use any other Apple's USB-C chargers). This is truly a tragedy.

# Bottom line

Even if we ensure that everyone can repair their devices - how can we be sure that the waste (broken screen, battery) is correctly recycled? Apple is doing everything to recycle every single part of the iPhone that they get their hands on, and new iPhones are using components made from recycled materials. After all, not everything is black and white.

# Links

- [What is Right to Repair by Louis Rossman](https://www.youtube.com/watch?v=Npd_xDuNi9k)
- [LMG Clips - EU introduces Right to Repair Legislation](https://www.youtube.com/watch?v=bfYy6hxm6sA)
- [iFixIt - Top 5 Right to Repair Wins of 2019](https://www.youtube.com/watch?v=1D7rOEtNCmM)
- [The Problem with E-Waste by Global Citizen](https://www.youtube.com/watch?v=FmJFVmtWf-I)
- [World's Largest Electronic Waste Dump by Simon Wilson](https://www.youtube.com/watch?v=AOa-hFG3L88)

# Resources

1. iPhone 12 and iPhone 12 Pro - [cnet.com](https://www.cnet.com/news/iphone-12-review-apple-smartphone-redesign-5g-four-models/)
1. iPhone 12 Unboxing by Marques Brownlee - [youtube.com](https://www.youtube.com/watch?v=Sx6dAx7dnXg)
1. Magsafe charger on iPhone - [appleinsider.com](https://appleinsider.com/articles/20/10/27/magsafe-15w-fast-charging-restricted-to-apple-20w-adapter)
