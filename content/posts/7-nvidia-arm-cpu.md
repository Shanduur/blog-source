+++
author = "Mateusz Urbanek"
title = "Nvidia and ARM revolution"
date = "2021-04-19"
description = "GTC 2021 reaction"
tags = [
    "arm",
    "gpu",
    "nvidia",
]
ShowToc = true
+++

Nvidia GPU Technology Conference 2021 took place on 12 April 2021. During this conference, Nvidia unveiled a technological roadmap for upcoming years, which included new GPU architectures, datacenter DPU improvements, and a new datacenter CPU.

# BlueField DPU

![dpu](https://www.servethehome.com/wp-content/uploads/2021/04/NVIDIA-GTC-2021-BlueField-Roadmap.jpg)
*Nvidia BlueField 2021 roadmap, ServeTheHome*

First, let's talk about NVidia DPU. It's a specialized piece of silicon, previously known as Smart NIC (Network Interface Card) - a network adapter that accelerates functionality and offloads it from the server (or storage) CPU. The new BlueField DPU includes ARM cores, CUDA cores, PCI-e 5.0 interface, and DDR5 memory. That's a huge step forward, resulting in up to 10x performance increase according to NVidia.

# Next generations of GPU

![gpu-next](https://cdn.wccftech.com/wp-content/uploads/2021/04/NVIDIA-GPU-CPU-Roadmap.png)
*Full Nvidia 2021 roadmap, Wccftech*

During the keynote, NVidia presented a roadmap for upcoming GPUs. We can expect two next generations of GPU in 2022 and 2024. Those will probably be named Lovelace (shown as Ampere Next) and Hopper (shown as Ampere Next Next). All leaks suggest that Lovelace will be the last monolithic die, and starting from Hopper, NVidia will move to chiplet design, which is much more cost-effective compared to the current production method.

# Grace CPU

![cpu-grace](https://www.servethehome.com/wp-content/uploads/2021/04/NVIDIA-GTC-2021-Grace-Bandwidth.jpg)
*Nvidia Grace CPU/SOC, ServeTheHome*

The most exciting part (at least in my opinion) was the presentation of Grace datacentre CPU. It's based on the ARMv9 architecture, which includes many enhancements over ARMv8, like ARM Confidential Compute Architecture. Additionally, NVidia unveiled that they will build a supercomputer for the Swiss National Supercomputing Centre in collaboration with Hewlett-Packard.

# ARM revolution

The push towards ARM is visible. After Amazon introduced EC2 instances based on Graviton CPUs, Apple introduced M1 chips, and Microsoft with Qualcomm showed SQ-series CPUs, NVidia presented their big, new, and shiny guns. Even if Microsoft Windows still lacks support and performance on ARM, both Linux and macOS are showed that this transition can be both fast and easy.

# Future of x86/AMD64 

But what does this mean for x86 platforms? No, they won't die. Cloud still heavily relies on x86 CPUs. There are still old and monolithic software systems that will have to be rewritten to the new architecture. And that change won't happen overnight. Additionally, both Intel and AMD had previously experimented with ARM. I'm sure their brilliant engineers are working on the way of adapting ARM or (maybe) trying to find a way to compete against it.