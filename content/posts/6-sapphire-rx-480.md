+++
author = "Mateusz Urbanek"
title = "Broken Sapphire RX 480 Nitro"
date = "2020-12-18"
description = "I got borken Sapphire RX 480 Nitro. Can I fix it?"
tags = [
    "amd",
    "gpu",
    "diy",
]
ShowToc = true
+++

So recently I laid my hands on this AMD Radeon RX 480. This is 4GB version from Sapphire, Nitro edition. Firstly I took a look on the PCB, cleaned it a little bit, removed solder from the place where someone accidentally removed one of capacitors, added some low quality thermal paste (Thermal Sliver AG).

![Damage on the PCB](/img/6/damage.png)

## Alpine

I connected it via PCI-e riser to the test rig (*AMD Athlon 64 x2 5200+* and *Gigabyte GA-M57SLI-S4*). Obviously, no signal on the monitor, but when I connected the second GPU (*Gigabyte Radeon HD 3650*) to get the display working, I stared Alpine (3.12.3, x86 image) and noticed some funky stuf in the dmesg:

```
[drm:amdgpu_init [amdgpu]] *ERROR* VGACON disables amdgpu kernel...
```

## Xubuntu

I switched to the Xubuntu, as I thought it might be an issue with the 32-bit operatin system. I started the testbench, and checked dmsg, which shown the same message:

```
amdgpu 0000:02:00.0: [drm:amdgpu_ring_test_helper [amdgpu]] *ERROR* ring kiq_2.1.0 test failed (-110) 
[drm:amdgpu_device_ip_init [amdgpu]] ERROR hw init of IP block <gfx_V8_0> failed -110
```

I decided to check lshw, to see if anything is reported here

```
*-display UNCLAIMED

description: VGA compatible controller

product: Ellesmere (Radeon RX 470/480/570/570X/580/580X/590)

vendor: Advanced Micro Devices, Inc. (AMD/ATI)

physical id: 0 bus info: pci@0000:02:00.0

version: e7

width: 64 bits

capabilities: pm pciexpress msi vga_controller cap_list 
configuration: latency=0
resources: memory: c0000000-cfffffff memory: 00000000-d01fffff ioport: 8000(size=256) memory: fb000000-fbo3ffff memory: fa000000-fa01
```

Okay, so at least the GPU is recognized correctly. Now I got curious - what `-110` means? Thanks to the **ThePCGeek** from [Techno Tim's](https://www.youtube.com/channel/UCOk-gHyjcWZNj3Br4oxwh0A) Discrod server, who sent me a [Radeon Open Compute issue](https://github.com/RadeonOpenCompute/ROCm/issues/1038), which suggested that the PSU is not powerfull enough. Yeah... Rabbit hole.

## Proxmox

So I connected broken GPU to my workstation (*Huananzhi X99-TF*, *Intel Xeon E5-2678v3*, *MSI GeForce GT710*)... What could go wrong? To my supprise, nothing burned or exploded, so *success*? And there is no `-110` in dmesg! Instead of it I recieved another bunch of messages, that looked like this:

```
AER: PCIe Bus Error: severity=Corrected, type=Data Link Layer, (Receiver ID)
    device [8086:2f02] error status/mask=00000080/00002000
        [7] BadDLLP
```

It looked like one of common [X99 and NVidia PCI-e bus error](https://www.overclock.net/threads/question-for-x99-board-owners-with-nvidia-cards-do-you-see-pcie-bus-errors-please-respond-to-poll.1539708/), so I hot unplugged the riser (nothing broken? Nice.), and recieved this:

```
AER: PCIe Bus Error: severity=Uncorrected (Fatal), type=Transaction Layer, (Receiver ID)
AER: device (8086:2f02) error status/mask=00000020/00000000
AER: [ 5] SDES (First)
```

To be continued...