+++
author = "Mateusz Urbanek"
title = "Hello!"
date = "2020-10-07"
description = "First post - about me"
tags = [
    "hello",
    "about me",
]
ShowToc = true
+++

# Good to see you there!

I am a student at the Silesian University of Technology, at the time of writing this post, I'm during my final (7th) semester of Electronic, Control, and Information Engineering major. Currently, I'm employed by [Asseco Poland](https://pl.asseco.com/en/).

# Interests? Many!

## Servers

I love to work with the servers. My final thesis project is **pluggabl** - app for distributed image processing. I will share it's code on GitHub when it's finished, but until this moment, I can tell that it is using gRPC for Client / Server communication and it's written in GoLang.

## Retro computer hardware

Another thing I'm passionate about is retro computers. Especially Apple and IBM machines. My dream is to put my hands on Apple Lisa and IBM 3270 terminal.

![apple-lisa](https://upload.wikimedia.org/wikipedia/commons/7/7d/Apple-LISA-Macintosh-XL.jpg)
*Image 1: Apple Lisa - Macintosh XL*

![ibm-3270](https://ajk.me/building-an-ibm-3270-terminal-controller/card-1000x750.jpg)
*Image 2: IBM 3270 terminal*

Unfortunately, the oldest thing in my collection is Intel Celeron 333Mhz on a 650B motherboard and NVidia GeForce2 MX400 graphics processing unit. I'm still missing a good looking case and hard drive - probably I will be using IDE to SD Card adapter instead of classic HDD as it will be much more reliable and easier to find. 

![ide=sd](https://images-na.ssl-images-amazon.com/images/I/61KcEm4UbvL._AC_SX466_.jpg)
*Image 3: IDE to SD Card adapter*

## Electric cars

Electric cars are the future. Although overall emission rates and waste production are not that much smaller than in the conventional gasoline cars (we have to produce that electricity in some way), this type of vechicles is pure futre. It's possible to generate electricity without any fossil fuels, even if we are far away from this point. Also electric cars have some drawbacks, even environmental - batteries are not easy to recycle, and we are producing more and more every year. But I'm sure that those obstacles will be ommited, especially looking at the progress of electric car manufacturers like Tesla or Lucid.

![tesla-model-s](https://static-assets.tesla.com/configurator/compositor?&options=$WTAS,$PPSW,$MTS07&view=STUD_3QTR_V2&model=ms&size=1920&bkba_opt=1&version=v0028d202010151040&crop=0,0,0,0&version=v0028d202010151040)
*Image 4: Tesla Model S*

# Resources
1. Apple Lisa - [wikipedia.org](https://en.wikipedia.org/wiki/Macintosh_XL#/media/File:Apple-LISA-Macintosh-XL.jpg)
1. IBM 3270 terminal - [ajk.me](https://ajk.me/building-an-ibm-3270-terminal-controller)
1. IDE to SD adapter - [amazon.co.uk](https://www.amazon.co.uk/2-5in-Adapter-Drive-Memory-Windows-default/dp/B084X5P9VT)
1. Tesla Model S - [tesla.com](https://www.tesla.com/en_US/models/design?redirect=no#battery)