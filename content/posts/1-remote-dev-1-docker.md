+++
author = "Mateusz Urbanek"
title = "My remote development tools (part 1) - Docker"
date = "2020-10-16"
description = "Remote development setup with Docker container engine."
tags = [
    "docker",
    "remote-dev",
    "containers",
]
ShowToc = true
+++

As most of the programs that I am developing are CLI tools or servers, and they don't feature any Graphical User Interface, I recently started my developing inside Docker containers. As I own some low power server, that is fast and stable enough to handle compiling software inside Docker, I decided that I will host this myself. In this miniseries, I will present how I created my setup, and I will explain why I did those things.

# Why Docker?

Docker is an industry-standard for the containerization of applications, and probably most of the developers at least once had contact with it. Its primary idea was to host applications in virtualized and isolated environments. In my opinion, with the use of many handful features, Docker can create a portable dev environment, that will improve our coding experience, and allow us to learn Docker a little bit more.

# How to create Dockerfile for remote development

First, we have to define the base for our container - in my opinion, Ubuntu is a good choice, as it is easy to play with and is quite stable - especially the LTS releases.

```Dockerfile
FROM ubuntu:focal
```

Firstly we should start by updating packages.

```Dockerfile
RUN apt-get update && apt-get upgrade -y
```

Then we are installing an SSH server - it will allow us to develop remotely inside the container.

```Dockerfile
RUN apt-get install -y openssh-server
```
Ubuntu comes in bare minimum form into our container, and it's good practice to ensure that some essential tools are installed on our environment.

```Dockerfile
RUN apt-get install -y neovim clang python3 git
```

Next, we should configure the SSH server and ensure that `prohibit-password` is set, as we will be using the root account, and we won't be using a password to log into our container.

```Dockerfile
RUN mkdir /var/run/sshd
RUN sed -i 's/#*PermitRootLogin prohibit-password/PermitRootLogin prohibit-password/g' /etc/ssh/sshd_config
```
Then we will create a data folder that will store our project.

```Dockerfile
RUN mkdir /root/data
```
For my convenience, I am also creating a custom bash prompt. You should change this to whatever suits you best. If you have trouble with writing something by hand, try to use this tool - [ezprompt.net](http://ezprompt.net/).

```Dockerfile
RUN echo 'export PS1="\u@\H:\n\$ "' >> /root/.bashrc
RUN echo "export PATH=$PATH" >> /root/.bashrc 
```
This step is crucial - SSH login fix. Without it, the user is kicked off after login.

```Dockerfile
RUN sed -i 's@session\s*required\s*pam_loginuid.so@session optional pam_loginuid.so@g' /etc/pam.d/sshd
ENV NOTVISIBLE="in users profile"
RUN echo "export VISIBLE=now" >> /etc/profile
```

This arg has to be set manually, for example with docker-compose. We are unable to login with a password to the container - so we have to provide a SSH public key from our machine.

```Dockerfile
ARG SSH_AUTH_KEY
RUN mkdir /root/.ssh && echo $SSH_AUTH_KEY > /root/.ssh/authorized_keys
```

Finally, we are exposing a port for SSH:

```Dockerfile
EXPOSE 22
```
And the entry point - this will be run every time our container is started.

```Dockerfile
ENTRYPOINT ["/usr/sbin/sshd", "-D"]
```

# Complete Dockerfile

```Docker
FROM ubuntu:focal

RUN apt-get update && apt-get upgrade -y

RUN apt-get install -y openssh-server

RUN apt-get install -y neovim clang python3 git

RUN mkdir /var/run/sshd
RUN sed -i 's/#*PermitRootLogin prohibit-password/PermitRootLogin prohibit-password/g' /etc/ssh/sshd_config

RUN mkdir /root/data

RUN echo 'export PS1="\u@\H:\n\$ "' >> /root/.bashrc
RUN echo "export PATH=$PATH" >> /root/.bashrc

RUN sed -i 's@session\s*required\s*pam_loginuid.so@session optional pam_loginuid.so@g' /etc/pam.d/sshd
ENV NOTVISIBLE="in users profile"
RUN echo "export VISIBLE=now" >> /etc/profile

ARG SSH_AUTH_KEY
RUN mkdir /root/.ssh && echo $SSH_AUTH_KEY > /root/.ssh/authorized_keys

EXPOSE 22

ENTRYPOINT ["/usr/sbin/sshd", "-D"]
```

# Docker-compose used to run Dockerfile

After creating Dockerfile, we can define the docker-compose YAML file to simplify the process of creating our container. It helps us create volume, bind the custom port to the exposed one, and set a build argument with our public RSA key.

```yaml
version: '3.8'
services:
  dev:
    build:
      context: .
      args:
        SSH_AUTH_KEY: 'ssh-rsa HERE_RSA mail@example.com'
    restart: unless-stopped
    volumes: 
      - data:/root/data
    ports:
      - '9922:22'

volumes:
  data:
```