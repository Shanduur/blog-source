+++
author = "Mateusz Urbanek"
title = "The movie you should watch - A Life on Our Planet"
date = "2020-10-20"
description = "A \"witness statement\", through which Attenborough shares first-hand his concern for the current state of the planet due to humanity's impact on nature and his hopes for the future."
tags = [
    "natural-history",
    "environmentalism",
]
ShowToc = true
+++

I never talked about the natural environment in public. But the educational value of this movie is that big that I can't just watch it and pass by without sharing it with anyone. Maybe some of you already watched *David Attenborough: A Life on Our Planet* recently added to Netflix. It tells a story about the natural environment, which surrounds us, and on which we have a devastating impact. Sir David Attenborough is a legendary narrator, traveler, and popularizer of natural science in the whole world. As a kid, thanks to my parents and grandparents, I was able to watch movies with him. Even though my contact with the natural sciences is negligible, I'm still, from time to time, watching documental series like *The Life of Mammals*, *The Life of Birds* or *The Blue Planet* with the sentiment.

![sir-david](https://cdn.cinemacloud.co.uk/imageFilm/146_1_2.jpg)
*Image 1: Poster of A Life on Our Planet*

# Why I wrote that lengthy introduction?

*A Life on Our Planet* is recorded as a "witness statement" of Sir David Attenborough. The movie describes the changes witnessed by him during his 93-years life. Deforestation of rainforests, excessive fish catching, or killing whales - these are just some of the production that he and we have witnessed. The movie draws the black screenplay, which we have written ourselves and which is currently being fulfilled. However, it also shows what we can do to fight for **our** survival on Earth. The film makes us reflect, rethink our decisions and choices. And another great advantage of the Polish version of this film is a great lector, a legendary voice associated with all the best nature films - Krystyna Czubówna.

![our-planet](https://www.ourplanet.com/uploads/394_fb-whale_desktop.jpeg)
*Image 2: Picture of Humpback Whale from Our Planet series*

# To sum up...

There is also a good and relatively short follow-up to Sir David's testimony (if you don't have time to make up for the whole filmography) - a documentary series - *Our Planet*, which is also available on Netflix. It is worth to get the free month for watching this movie and series if you haven't used this platform yet. If you are not convinced about Netflix - you can always search for it on the Internet, and maybe you will find it 😏.

*Our Planet* is also available [for free on YouTube](https://www.youtube.com/playlist?list=PL7rb3uMaYmjHqT_JUcQYCBa4nEtfDKuSa)!

{{< youtube GfO-3Oir-qM >}}

# Links

- EN:
   - [David Attenborough: A Life on Our Planet](https://www.netflix.com/pl-en/title/80216393)
   - [Our Planet - Netflix](https://www.netflix.com/pl-en/title/80049832)
   - [Our Planet - YouTube](https://www.youtube.com/playlist?list=PL7rb3uMaYmjHqT_JUcQYCBa4nEtfDKuSa)
   - [AttenboroughFilm.com](https://attenboroughfilm.com/)
   - [OurPlanet.com](https://www.ourplanet.com/en/)
   - [WWF - WorldWildLife](https://www.worldwildlife.org/)
- PL:
   - [David Attenborough: Życie na naszej planecie](https://www.netflix.com/pl/title/80216393)
   - [Nasza Planeta - Netflix](https://www.netflix.com/pl/title/80049832)

# Resources

1. A Life on Our Planet Poster - [ArchlightCinema.co.uk](https://www.archlightcinema.co.uk/film/David-Attenborough:-A-Life-On-Our-Planet)
2. Humpback Whale from Our Planet - [OurPlanet.com](https://www.ourplanet.com/en/video/five-steps-to-help-save-our-planet/)